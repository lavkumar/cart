const mongoose = require('mongoose');

const categoriesSchema = new mongoose.Schema({
    name:String,
    description:String,
    Groups :{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Groups'
    },
    isactive:Boolean,
   
});

const category= mongoose.model('Categories', categoriesSchema);

module.exports = category;