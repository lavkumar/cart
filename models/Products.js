const mongoose = require('mongoose');

const groupSchema = new mongoose.Schema({
    name: String,
    description: String,
    isactive:Boolean,
    Categories :{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Categories'
    }
   
});

const product = mongoose.model('Proucts', groupSchema);

module.exports = product;