const Group1 = require('../models/Groups');
const Ga = require('../models/Categories');
const pro = require('../models/products');
const express = require("express");
var router = express.Router();


// creating products
//posting data in database
router.post('/product', (req, res) => {
    const cat = new pro(req.body);
    cat.save().then((cat) => {
        res.status(201).send(cat);
    }).catch((error) => {
        res.status(400).send(error);
    })
})
// getting data freom database
router.get('/product', (req, res) => {
    pro.find({id:req.params._id}).then((cat) => {
        res.send(cat);
    }).catch((error) => {
        res.status(500).send(error);
    })
})



// creating category
//posting data in database
router.post('/category', (req, res) => {
    const cat = new Ga(req.body);
    cat.save().then((cat) => {
        res.status(201).send(cat);
    }).catch((error) => {
        res.status(400).send(error);
    })
})
// getting data freom database
router.get('/category', (req, res) => {
    Ga.find({id:req.params._id}).then((cat) => {
        res.send(cat);
    }).catch((error) => {
        res.status(500).send(error);
    })
})




//creating Groups
//posting data in database
router.post('/blogs', (req, res) => {
    const bla = new Group1(req.body);
    bla.save().then((bla) => {
        res.status(201).send(bla);
    }).catch((error) => {
        res.status(400).send(error);
    })
})
// getting data freom database
router.get('/blogs', (req, res) => {
    Group1.find().then((bla) => {
        console.log(bla);
        res.send(bla);
        
    }).catch((error) => {
        res.status(500).send(error);
    })
})

module.exports = router;